// HomeCore.swift
// MovieDB
//
// Created by Luis Pineda on 28/07/21.
// 
//

import Foundation
import ComposableArchitecture

enum movieType {
    case popular, toprated, search
}

//MARK:- State
struct HomeState: Equatable {
    var loading: Bool = false
    var type: movieType = .popular
    var moviePage: MoviePageModel = .mock
    
    var search: SearchState?
    
    //TODO: Esta parte de los filtros puede estar directamente en un componente
    // aparte para que la vista principal no tenga la tarea de realizar los filtros
    var fielters: [String: String] = ["page":"1"]
    var filterLanguage: Iso639_1 = .en
    var filteradult: Bool = false
    
    var detailState: MovieDetailState?
}

//MARK:- Action
enum HomeAction: Equatable {
    case load
    case typeChange(movieType)
    case movies
    case movieResponse(Result<MoviePageModel,ErrorMessage>)
    case nextPage
    
    case searchTapped
    case search(SearchAction)
    
    case languageChange(Iso639_1)
    case adultChange(Bool)
    
    case detailActions(MovieDetailAction)
    case setNavigation(selection: Int?)
}

//MARK:- Environment
struct HomeEnvironment {
    var movieClient = MovieClient.live
    var mainQueue = DispatchQueue.main.eraseToAnyScheduler()
}

extension HomeEnvironment {
    var detail: MovieDetailEnvironment {
        .init(movieClient: movieClient,
              mainQueue: mainQueue)
    }
}

//MARK:- Reducer
let homeReducer = Reducer<HomeState, HomeAction, HomeEnvironment>.combine(
    searchReducer.optional().pullback(
        state: \.search,
        action: /HomeAction.search,
        environment: { _ in SearchEnvironment() }
    ),
    movieDetailReducer.optional().pullback(
        state: \.detailState,
        action: /HomeAction.detailActions,
        environment: \.detail
    ),
    Reducer{
        state, action, environment in
        struct MovieId: Hashable {}
        switch action {
        case .load:
            state.loading = true
            return environment
                .movieClient
                .movies(state.fielters, state.type)
                .receive(on: environment.mainQueue)
                .catchToEffect()
                .map(HomeAction.movieResponse)
                .cancellable(id: MovieId())
        case .movies:
            state.loading = true
            return environment
                .movieClient
                .movies(state.fielters, state.type)
                .receive(on: environment.mainQueue)
                .catchToEffect()
                .map(HomeAction.movieResponse)
                .cancellable(id: MovieId())
        case .movieResponse(.success(let page)):
            state.loading = false
            if page.page == 1 {
                state.moviePage = page
            } else {
                state.moviePage.set(page: page.page,
                                    movies: page.movies,
                                    totalPages: page.totalPages,
                                    totalResults: page.totalResults)
            }
            return .none
        case .movieResponse(.failure(let error)):
            state.loading = false
            //TODO: en este punto se tiene que hacer el manejo del error, nose mostrarlo en una alerta puede ser
            print("Error: \(error)")
            return .none
        case .typeChange(let type):
            state.type = type
            state.fielters["page"] = "1"
            return Effect(value: .movies)
        case .nextPage:
            let nextPage = state.moviePage.page + 1
            if nextPage <= state.moviePage.totalPages {
                state.fielters["page"] = "\(nextPage)"
                return Effect(value: .movies)
            }
            return .none
        case .searchTapped:
            state.search = .init(query: "")
            state.type = .search
            state.moviePage = .init()
            state.fielters["page"] = "1"
            return .none
        case .search(.queryChanged(let query)):
            state.moviePage = .init()
            state.fielters["page"] = "1"
            state.fielters["query"] = query.lowercased()
            return .concatenate(.cancel(id: MovieId()),
                                Effect(value: .movies))
            
        case .search(.cancel):
            state.search = nil
            state.type = .popular
            state.moviePage = .init()
            state.fielters["page"] = "1"
            return Effect(value: .movies)
        case .languageChange(let language):
            state.filterLanguage = language
            state.fielters["with_original_language"] = language.rawValue.lowercased()
            return Effect(value: .movies)
        case .adultChange(let adult):
            state.filteradult = adult
            state.fielters["include_adult"] = "\(adult)"
            return Effect(value: .movies)
        case .setNavigation(selection: let id):
            guard let id = id else {
                return .none
            }
            state.detailState = .init(id: id)
            return .none
        case .detailActions(.onDisappear):
            state.detailState = nil
            return .none
        case .detailActions(_):
            return .none
        }
    }
)
