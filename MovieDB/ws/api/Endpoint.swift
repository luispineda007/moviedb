//
//  Endpoint.swift
//  MovieDB
//
//  Created by Luis Pineda on 28/07/21.
//

import Foundation


struct Endpoint {
    
    static let schemeAPI = "https"
    static let hostAPI = "api.themoviedb.org"
    static let pathAPI = "/3/"
    static let apiKey = "9a23022087340633794842e0d8b9ce27"
    static private let baseImagen = "https://image.tmdb.org/t/p/w500/%@"
    
    private enum EndpointType: String {
        case popular = "movie/popular"
        case topRated = "movie/top_rated"
        case search = "search/movie"
        case detail = "movie/%d"
    }
    
    static private func createURLComponents(parameters: [String: String], endpoint: String) -> URL {
        var components = URLComponents()
        components.scheme = Endpoint.schemeAPI
        components.host   = Endpoint.hostAPI
        components.path   = Endpoint.pathAPI + endpoint
        components.queryItems = [URLQueryItem]()
        components.queryItems!.append(URLQueryItem(name: "api_key", value: Endpoint.apiKey))
        components.queryItems!.append(URLQueryItem(name: "language", value: "en-US"))
        if !parameters.isEmpty {
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: value)
                components.queryItems!.append(queryItem)
            }
        }
        print(components)
        return components.url!
    }
    static func popular(_ parameters: [String: String]) -> URLRequest {
        return URLRequest(url: createURLComponents(parameters: parameters, endpoint: EndpointType.popular.rawValue))
    }
    
    static func topRated(_ parameters: [String: String]) -> URLRequest {
        return URLRequest(url: createURLComponents(parameters: parameters, endpoint: EndpointType.topRated.rawValue))
    }
    
    static func discover(_ parameters: [String: String]) -> URLRequest {
        return URLRequest(url: createURLComponents(parameters: parameters, endpoint: EndpointType.search.rawValue))
    }
    
    static func detail(id: Int) -> URLRequest {
        return URLRequest(url: createURLComponents(parameters: [:], endpoint: String(format: EndpointType.detail.rawValue, id)))
    }
    
    static func urlImage(path: String) -> URL {
        return URL(string: String(format: baseImagen, path))!
    }
}
