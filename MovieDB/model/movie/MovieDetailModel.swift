//
//  MovieDetailModel.swift
//  MovieDB
//
//  Created by Luis Pineda on 2/08/21.
//

import Foundation


// MARK: - MoveDetailModel
struct MovieDetailModel: Codable, Equatable {
    let id: Int
    let adult: Bool
    let budget: Int
    let genres: [GenreModel]
    let imdbID, originalLanguage, originalTitle, overview: String
    let popularity: Double
    let posterPath: String?
    let productionCompanies: [ProductionCompany]
    let releaseDate: String
    let revenue, runtime: Int
    let spokenLanguages: [SpokenLanguage]
    let status, tagline, title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int

    enum CodingKeys: String, CodingKey {
        case adult
        case budget, genres, id
        case imdbID = "imdb_id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case releaseDate = "release_date"
        case revenue, runtime
        case spokenLanguages = "spoken_languages"
        case status, tagline, title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}

// MARK: - Genre
struct GenreModel: Codable, Equatable {
    let id: Int
    let name: String
}

// MARK: - ProductionCompany
struct ProductionCompany: Codable, Equatable {
    let id: Int
    let logoPath: String?
    let name, originCountry: String

    enum CodingKeys: String, CodingKey {
        case id
        case logoPath = "logo_path"
        case name
        case originCountry = "origin_country"
    }
}


// MARK: - SpokenLanguage
struct SpokenLanguage: Codable, Equatable {
    let englishName, iso639_1, name: String

    enum CodingKeys: String, CodingKey {
        case englishName = "english_name"
        case iso639_1 = "iso_639_1"
        case name
    }
}

//MARK:- Mock
#if DEBUG

extension Array where Element == GenreModel {
    static func mock(_ n: Int) -> Array {
        (1..<n+1).map {
            GenreModel(id: $0, name: "Genre\($0)")
        }
    }
}

extension Array where Element == ProductionCompany {
    static func mock(_ n: Int) -> Array {
        (1..<n+1).map {
            ProductionCompany(id: $0,
                              logoPath: "/8lvHyhjr8oUKOOy2dKXoALWKdp0.png",
                              name: "Universal Pictures",
                              originCountry: "US")
        }
    }
}

extension Array where Element == SpokenLanguage {
    static func mock(_ n: Int) -> Array {
        (1..<n+1).map { _ in
            SpokenLanguage(englishName: "English",
                           iso639_1: "en",
                           name: "English")
        }
    }
}

extension MovieDetailModel {
    static var mock = MovieDetailModel(id: 1,
                                       adult: false,
                                       budget: 0,
                                       genres: .mock(2),
                                       imdbID: "tt5433140",
                                       originalLanguage: "en",
                                       originalTitle: "Fast & Furious 10",
                                       overview: "The tenth installment in the Fast Saga.",
                                       popularity: 829.77,
                                       posterPath: "/2DyEk84XnbJEdPlGF43crxfdtHH.jpg",
                                       productionCompanies: .mock(2),
                                       releaseDate: "",
                                       revenue: 0,
                                       runtime: 0,
                                       spokenLanguages: .mock(2),
                                       status: "In Production",
                                       tagline: "",
                                       title: "Fast & Furious 10",
                                       video: false,
                                       voteAverage: 0,
                                       voteCount: 0)

}
#endif
