//
//  MovieCellView.swift
//  MovieDB
//
//  Created by Luis Pineda on 28/07/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct MovieCellView: View {
    
    private let widthT: CGFloat = UIScreen.main.bounds.size.width
    private let shadowOpacity: Double = 0.2
    private let shadowRadius: CGFloat = 3
    private let shadowY: CGFloat = 2
    
    var movie: MovieModel
    var action: () -> ()
    
    var body: some View {
        Button(action: {
            action()
        }, label: {
            VStack(alignment: .center, spacing: 5) {
                WebImage(url: Endpoint.urlImage(path: movie.posterPath ?? ""))
                    .resizable() // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                    .placeholder(Image("placeholder")) // Placeholder Image
                    .indicator(.activity) // Activity Indicator
                    .animation(.easeInOut(duration: 0.5)) // Animation Duration
                    .transition(.fade) // Fade Transition
                    .frame(width: widthT * 0.45, height: widthT * 0.45)
                    .aspectRatio(1, contentMode: .fit)
                    .overlay(
                        HStack(spacing: 3.0) {
                            Image(systemName: "star.fill")
                            Text("\(movie.voteAverage, specifier: "%.1f")")
                        }.padding(2)
                        .textFont(color: .yellow, decoration: .bold)
                        .background(Color.black.opacity(0.5))
                        .cornerRadius(radius: 5, corners: [.bottomLeft])
                        ,alignment: .topTrailing)
                
                Text(movie.title)
                    .lineLimit(1)
                    .mediumTextFont()
                    .padding(.horizontal, 10)
                    .padding(.bottom, 5)
            }
            .frame(width: widthT * 0.45)
            .background(Color.pBackgroundCard)
            .cornerRadius(5)
            .shadow(color: Color.black.opacity(shadowOpacity),
                    radius: shadowRadius,
                    x: .zero,
                    y: shadowY)
        })
    }
}

struct MoveCellView_Previews: PreviewProvider {
    static var previews: some View {
        MovieCellView(movie: .mock, action: {})
    }
}
