//
//  MoviePageModel.swift
//  MovieDB
//
//  Created by Luis Pineda on 29/07/21.
//

import Foundation

struct MoviePageModel: Codable, Hashable {
    var page: Int
    var movies: [MovieModel]
    var totalPages, totalResults: Int
    
    enum CodingKeys: String, CodingKey {
        case page
        case movies = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
    
    init() {
        page = 1
        movies = []
        totalPages = 0
        totalResults = 0
    }
    
    init(page: Int,
         movies: [MovieModel],
         totalPages: Int,
         totalResults: Int) {
        self.page = page
        self.movies = movies
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
    
    mutating func set(page: Int,
                      movies: [MovieModel],
                      totalPages: Int,
                      totalResults: Int) {
        self.page = page
        self.movies.append(contentsOf: movies)
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
    
}

//MARK:- Mock
#if DEBUG
extension MoviePageModel {
    static var mock = MoviePageModel(page: 1,
                                     movies: .mock(8),
                                     totalPages: 1,
                                     totalResults: 1)
    static func mock(page: Int,
                     movies: [MovieModel],
                     totalPages: Int,
                     totalResults: Int) -> MoviePageModel {
        MoviePageModel(page: page,
                       movies: movies,
                       totalPages: totalPages,
                       totalResults: totalResults)
    }
}
#endif
