//
//  MovieDBApp.swift
//  MovieDB
//
//  Created by Luis Pineda on 28/07/21.
//

import SwiftUI
import ComposableArchitecture

@main
struct MovieDBApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView(
                store: Store(
                    initialState: HomeState(),
                    reducer: homeReducer,
                    environment: HomeEnvironment()
                )
            )
        }
    }
}
