//
//  MovieClient.swift
//  MovieDB
//
//  Created by Luis Pineda on 29/07/21.
//

import ComposableArchitecture
import Foundation

struct MovieClient {
    var movies: ([String: String], movieType) -> Effect<MoviePageModel, ErrorMessage>
    var detail: (Int) -> Effect<MovieDetailModel, ErrorMessage>
}

// MARK: - LIVE
extension MovieClient {
    static var live = MovieClient(
        movies: { parameters, type in
            switch type {
            case .popular:
                return MovieAPI.send(Endpoint.popular(parameters))
            case .toprated:
                return MovieAPI.send(Endpoint.topRated(parameters))
            case .search:
                return MovieAPI.send(Endpoint.discover(parameters))
            }
        },
        detail: { id in
            return MovieAPI.send(Endpoint.detail(id: id))
        }
    )
}

#if DEBUG
// MARK: - MOCK
extension MovieClient {
    static var mock = MovieClient(
        movies: {_, _ in Effect(value: .mock)},
        detail: {_ in Effect(value: .mock)}
    )
    
    static func mock(movies: @escaping ([String: String], movieType) -> Effect<MoviePageModel, ErrorMessage> = {_, _ in fatalError()},
                     detail: @escaping (Int) -> Effect<MovieDetailModel, ErrorMessage> = { _ in fatalError()}
    ) -> MovieClient {
        MovieClient(movies: movies,
                    detail: detail)
    }
}
#endif
