// HomeView.swift
// MovieDB
//
// Created by Luis Pineda on 28/07/21.
// 
//

import SwiftUI
import ComposableArchitecture

struct HomeView: View {
    
    let store: Store<HomeState, HomeAction>
    @State var isFilters: Bool = false
    let columns = [
        GridItem(.adaptive(minimum: 150))
    ]
    
    init(store: Store<HomeState, HomeAction>) {
        self.store = store
    }
    
    var body: some View {
        WithViewStore(store) { viewStore in
            NavigationView {
                ZStack {
                    Color.pBackground.ignoresSafeArea(.all)
                    VStack(spacing: 0.0) {
                        IfLetStore(
                            self.store.scope(
                                state: \.search,
                                action: HomeAction.search
                            ),
                            then: SearchBarView.init(store:)
                        )
                        .transition(AnyTransition.move(edge: .top))
                        .animation(.easeIn)
                        .padding(.vertical, 8)
                        .padding(.horizontal, 15)
                        if !viewStore.moviePage.movies.isEmpty {
                            ScrollPageView(values: viewStore.moviePage.movies) {
                                viewStore.send(.nextPage)
                            } content: { movie in
                                MovieCellView(movie: movie) {
                                    viewStore.send(.setNavigation(selection: movie.id))
                                }
                            }
                        } else {
                            Spacer()
                        }
                        if viewStore.search == nil {
                            Rectangle().fill(Color.gray)
                                .frame(height: 1)
                            HStack {
                                ButtonTab(text: "Popular", isSelect: viewStore.type == .popular) {
                                    viewStore.send(.typeChange(.popular))
                                }
                                ButtonTab(text: "Top Rated", isSelect: viewStore.type == .toprated) {
                                    viewStore.send(.typeChange(.toprated))
                                }
                            }
                        }
                    }
                    if isFilters {
                        FiltersView(store: self.store, isFilters: $isFilters)
                    }
                    
                    NavigationLink(
                        destination: IfLetStore(self.store.scope(state: \.detailState,
                                                                 action: HomeAction.detailActions),
                                                then: MovieDetailView.init(store:)),
                        isActive: viewStore.binding(get: { $0.detailState != nil },
                                                    send: { _ in .setNavigation(selection: nil)}),
                        label: {
                            EmptyView()
                        })
                }.navigationBarColor(.pBackground)
                .resignKeyboardOnDragGesture()
                .navigationBarTitle(Text("MovieDB"), displayMode: .inline)
                .navigationBarItems(trailing:
                                        HStack {
                                            Button(action: { viewStore.send(.searchTapped) }) {
                                                Image(systemName: "magnifyingglass")
                                                    .padding(8)
                                            }.disabled(viewStore.search != nil)
                                            .buttonStyle(PlainButtonStyle())
                                            Button(action: {
                                                self.isFilters.toggle()
                                            }, label: {
                                                Image(systemName: "slider.horizontal.3")
                                            })
                                            
                                        }
                
                )
                .onAppear(perform: {
                    viewStore.send(.load)
                })
            }
        }
    }
    
    struct ButtonTab: View {
        let text: String
        var isSelect: Bool
        var action: () -> ()
        var body: some View {
            Button(action: {
                action()
            }, label: {
                HStack {
                    Spacer()
                    Text(text)
                        .textFont(color: isSelect ? Color.pPrimary : Color.gray)
                        .padding()
                    Spacer()
                }.background(Color.pBackground)
            })
            .overlay( Group {
                if isSelect {
                    Rectangle().fill(Color.pPrimary)
                        .frame(height: 3)
                } else {
                    EmptyView()
                }
            }
            ,alignment: .top)
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(
            store: Store(
                initialState: HomeState(),
                reducer: homeReducer,
                environment: HomeEnvironment()
            )
        )
    }
}
