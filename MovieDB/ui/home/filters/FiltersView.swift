//
//  FiltersView.swift
//  MovieDB
//
//  Created by Luis Pineda on 2/08/21.
//

import SwiftUI
import ComposableArchitecture

struct FiltersView: View {
    let padding: CGFloat = 20
    let store: Store<HomeState, HomeAction>
    
    @State var isOpacity: Bool = false
    @State var animation = false
    
    @State private var isPickerLanguage: Bool = false
    @Binding private var isFilters: Bool
    
    init(store: Store<HomeState, HomeAction>,
         isFilters: Binding<Bool>) {
        self.store = store
        _isFilters = isFilters
    }
    
    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                if self.$isOpacity.wrappedValue {
                    Color.pBackground.colorInvert().opacity(0.2)
                        .edgesIgnoringSafeArea(.all)
                        .zIndex(10)
                        .animation(.default)
                }
                VStack(spacing: .zero) {
                    VStack(spacing: .zero) {
                        Image(systemName: "slider.horizontal.3")
                            .frame(width: 50, height: 50)
                            .textFontSize(size:  40,
                                          color: Color.pPrimary,
                                          decoration: .bold)
                            .scaleEffect(animation ? 0.8 : 0.2)
                            .rotationEffect(animation ? .degrees(360) : .degrees(0))
                            .animation(Animation.easeIn(duration: 0.5))
                            .frame(width: 80, height: 80)
                            .background(Color.pBackground)
                            .clipShape(Circle())
                            .overlay(Circle()
                                        .stroke(Color.pBackground,
                                                lineWidth: 5))
                            .offset(y: -padding)
                            .padding(.top, -padding)
                            .onAppear {
                                self.animation = true
                                self.isOpacity = true
                            }
                        VStack( spacing: padding) {
                            if isPickerLanguage {
                                PickerLanguageView(store: self.store,
                                                   isPickerLanguage: $isPickerLanguage)
                            } else {
                                Toggle(isOn: viewStore.binding(get: {$0.filteradult},
                                                               send: HomeAction.adultChange)){
                                    Text("Adult Content ")
                                        .textFont()
                                }
                                HStack {
                                    Text("Original Language: ")
                                        .textFont()
                                    Spacer()
                                    Button(action: {
                                        self.isPickerLanguage = true
                                    }, label: {
                                        Text("\(viewStore.filterLanguage.language)")
                                            .textFont(decoration: .bold)
                                    })
                                    
                                }
                                HStack {
                                    Text("Ordenar : ")
                                        .textFont()
                                    Spacer()

                                }
                                
                                Button(action: {
                                    self.isFilters = false
                                }, label: {
                                    Text("Close")
                                })
                            }
                            
                        }.padding([.horizontal, .bottom], 25)
                    }
                    
                }.background(RoundedRectangle(cornerRadius: 10)
                                .fill(Color.pBackground)
                                .shadow(color: Color.pPrimaryText.opacity(0.2),
                                        radius: 3,
                                        x: .zero,
                                        y: 2))
                
                .padding(.horizontal, 30)
                .zIndex(15)
                
            }
            
            .onAppear(perform: {
                viewStore.send(.load)
            })
        }
    }
    
    struct PickerLanguageView: View {
        
        let store: Store<HomeState, HomeAction>
        @Binding private var isPickerLanguage: Bool
        
        init(store: Store<HomeState, HomeAction>,
            isPickerLanguage: Binding<Bool> ) {
            self.store = store
            _isPickerLanguage = isPickerLanguage
        }
        
        var body: some View {
            WithViewStore(store) { viewStore in
                HStack {
                    Spacer()
                    Button(action: {
                        self.isPickerLanguage = false
                    }, label: {
                        Image(systemName: "xmark")
                            .textFont()
                    })
                }
                Text("Original Language: ")
                Picker("Please choose a Language", selection: viewStore.binding(get: {$0.filterLanguage},
                                                                             send: HomeAction.languageChange)) {
                    ForEach(Iso639_1.allCases, id: \.self) {
                        Text($0.language)
                    }
                }
                Text("You selected: \(viewStore.filterLanguage.language)")
            }
        }
    }
    
}
