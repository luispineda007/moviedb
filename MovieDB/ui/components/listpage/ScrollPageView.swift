//
//  ScrollPageView.swift
//  MovieDB
//
//  Created by Luis Pineda on 29/07/21.
//

import SwiftUI

struct ScrollPageView<T,Content>: View where T:Identifiable, Content:View {
    
    let columns = [
        GridItem(.adaptive(minimum: 150))
    ]
    
    var values: [T]
    @State var isLoading: Bool = false
    var nextPage: () -> Void
    var content: (T) -> Content
    var emptyState: String
    
    init(values: [T],
         nextPage: @escaping () -> Void,
         content: @escaping (T) ->  Content,
         emptyState: String = "Sin Peliculas para mostrar") {
        self.values = values
        self.nextPage = nextPage
        self.content = content
        self.emptyState = emptyState
    }
    
    var body: some View {
        VStack {
            if values.count > .zero {
                ScrollView {
                    LazyVGrid(columns: columns, spacing: 20) {
                        ForEach(values) { value in
                            self.content(value)
                                .onAppear {
                                    self.isLoading = false
                                    self.listItemAppears(value)
                                }
                        }
                    }
                    if self.$isLoading.wrappedValue {
                        HStack {
                            Spacer()
                            LoadingView()
                            Spacer()
                        }
                    }
                }
            } else {
                Text(emptyState)
                    .textFont()
                    .padding()
            }
        }
    }
    
    func listItemAppears<Item: Identifiable>(_ item: Item) {
        if self.values.isLastItem(item) {
            self.isLoading = true
            self.nextPage()
        }
    }
}
