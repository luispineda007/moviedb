//
//  MovieModel.swift
//  MovieDB
//
//  Created by Luis Pineda on 28/07/21.
//

import Foundation

struct MovieModel: Identifiable, Codable, Hashable {
    var id: Int = .zero
    var title: String = ""
    var posterPath: String? = ""
    var voteAverage: Double = .zero
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case posterPath = "poster_path"
        case voteAverage = "vote_average"
    }
}

//MARK:- Mock
#if DEBUG
extension MovieModel {
    static var mock = MovieModel(id: 1,
                                 title: "Space Jam: A New Legacy",
                                 posterPath: "/5bFK5d3mVTAvBCXi5NPWH0tYjKl.jpg",
                                 voteAverage: 3.6)
    static func mock(id: Int,
                     title: String,
                     posterPath: String,
                     voteAverage: Double) -> MovieModel {
        MovieModel(id: id,
                   title: title,
                   posterPath: posterPath,
                   voteAverage: voteAverage)
    }
}

extension Array where Element == MovieModel {
    static func mock(_ n: Int) -> Array {
        (1..<n+1).map {
            MovieModel(id: $0,
                       title: "Movie \($0)",
                       posterPath: "/5bFK5d3mVTAvBCXi5NPWH0tYjKl.jpg",
                       voteAverage: 3.6)
        }
    }
    
    static func mock(_ n: Int, _ m: Int) -> Array {
        (n..<m+1).map {
            MovieModel(id: $0,
                       title: "Movie \($0)",
                       posterPath: "/5bFK5d3mVTAvBCXi5NPWH0tYjKl.jpg",
                       voteAverage: 3.6)
        }
    }
}
#endif
