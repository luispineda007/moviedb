// MovieDetailView.swift
// MovieDB
//
// Created by Luis Pineda on 2/08/21.
// 
//

import SwiftUI
import ComposableArchitecture
import SDWebImageSwiftUI

struct MovieDetailView: View {
    
    let width: CGFloat = UIScreen.main.bounds.size.width
    let store: Store<MovieDetailState, MovieDetailAction>
    
    init(store: Store<MovieDetailState, MovieDetailAction>) {
        self.store = store
    }
    
    var body: some View {
        WithViewStore(store) { viewStore in
            ScrollView {
                WebImage(url: Endpoint.urlImage(path: viewStore.detail.posterPath ?? ""))
                    .resizable() // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                    .placeholder(Image("placeholder")) // Placeholder Image
                    .indicator(.activity) // Activity Indicator
                    .animation(.easeInOut(duration: 0.5)) // Animation Duration
                    .transition(.fade) // Fade Transition
                    .aspectRatio(1, contentMode: .fill)
                    .frame(width: width)
                VStack(spacing: 20.0) {
                    Text(viewStore.detail.title)
                        .titleFont()
                        .padding(.bottom)

                    Text(viewStore.detail.overview)
                        .textFont()
                    
                    HStack {
                        Text ("Genres")
                            .textFont(decoration: .bold)
                        Spacer()
                    }

                    ScrollView(.horizontal) {
                        LazyHStack(alignment: .top, spacing: 10) {
                            ForEach(.zero..<viewStore.detail.genres.count, id: \.self) { index in
                                Text("\(viewStore.detail.genres[index].name)")
                                    .padding(.horizontal)
                                    .padding(.vertical, 6)
                                    .background(Color.pBackground)
                                    .cornerRadius(3.0)
                            }
                        }
                    }
                    
                    HStack {
                        Text ("Production Companies")
                            .textFont(decoration: .bold)
                        Spacer()
                    }
                    ScrollView(.horizontal) {
                        LazyHStack(alignment: .top, spacing: 10) {
                            ForEach(.zero..<viewStore.detail.productionCompanies.count, id: \.self) { index in
                                WebImage(url: Endpoint.urlImage(path: viewStore.detail.productionCompanies[index].logoPath ?? ""))
                                    .resizable() // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                                    .placeholder(Image("placeholder")) // Placeholder Image
                                    .indicator(.activity) // Activity Indicator
                                    .animation(.easeInOut(duration: 0.5)) // Animation Duration
                                    .transition(.fade) // Fade Transition
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                        }
                    }
                }.padding()
                .background(Color.pBackgroundCard)
                .cornerRadius(5)
                .padding(.top, -30)
                .padding(.horizontal)
            }.background(Color.pBackground)
            .onAppear(perform: {
                viewStore.send(.load)
            })
            .onDisappear(perform: {
                viewStore.send(.onDisappear)
            })
        }.ignoresSafeArea(.all)
    }
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetailView(
            store: Store(
                initialState: MovieDetailState(id: 233),
                reducer: movieDetailReducer,
                environment: MovieDetailEnvironment(movieClient: .mock)
            )
        )
    }
}
