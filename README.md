# MovieDB

_Prueba de iOS Scotibank Colpatria._

## Requisitos 📋

```
* iOS 14.0+
* Xcode 12.5
```

## Package Manager 📋
```
* Composable Architecture Swift
* Realm Swift
* SDWebimage swiftui
```


## Prueba ⚙️

*     Consume el API https://developers.themoviedb.org
*    Se debe poder visualizar 2 vistas con las categorias “Popular” y “TopRated” (Se sugiere usar un SegmentedControl).
*   Se debe poder filtrar por “adult”,  “original_language” y rango de “vote_average” (Se sugiere usar una vista para todos los filtros).
*    Se debe poder realizar la búsqueda de una película y visualizar los resultados agrupados por categorías.
*   Se deben implementar pruebas unitarias.


### Preview 🔧
###### Home
![Preview1](/Preview/IMG_1.jpeg)
![Preview2](/Preview/IMG_2.jpeg)

###### Search
![Preview1](/Preview/IMG_3.jpeg)

###### Filtes
![Preview1](/Preview/IMG_4.jpeg)
![Preview1](/Preview/IMG_5.jpeg)

###### Detail
![Preview1](/Preview/IMG_6.jpeg)

###### Cobertura de los Test realizados a la parte lógica del proyecto 

![Preview11](/Preview/IMG_7.jpeg)

