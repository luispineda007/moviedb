//
//  ColorExtension.swift
//  MovieDB
//
//  Created by Luis Pineda on 28/07/21.
//

import SwiftUI

extension Color {
    static let pBackground = Color("Background")
    static let pBackgroundCard = Color("BackgroundCard")
    static let pBackgroundTF = Color("BackgroundTextFiled")
    static let pPrimary = Color("Primary")
    static let pPrimaryText = Color("PrimaryText")
    static let pThird = Color("Third")
    static let pFourth = Color("Fourth")
    
}
extension UIColor {
    static let pBackground = UIColor(named: "Background")
    static let pBackgroundCard = UIColor(named: "BackgroundCard")
    static let pBackgroundTF = UIColor(named: "BackgroundTextFiled")
    static let pPrimary = UIColor(named: "Primary")
    static let pPrimaryText = UIColor(named: "PrimaryText")
    static let pThird = UIColor(named: "Third")
    static let pFourth = UIColor(named: "Fourth")
}
