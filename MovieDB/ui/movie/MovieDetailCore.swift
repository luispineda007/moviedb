// MovieDetailCore.swift
// MovieDB
//
// Created by Luis Pineda on 2/08/21.
// 
//

import Foundation
import ComposableArchitecture

//MARK:- State
struct MovieDetailState: Equatable {
    var id: Int
    var loading: Bool = false
    var detail: MovieDetailModel = .mock
}

//MARK:- Action
enum MovieDetailAction: Equatable {
    case load
    case detailResponse(Result<MovieDetailModel, ErrorMessage>)
    case onDisappear
}

//MARK:- Environment
struct MovieDetailEnvironment {
    var movieClient: MovieClient
    var mainQueue = DispatchQueue.main.eraseToAnyScheduler()
}

//MARK:- Reducer
let movieDetailReducer = Reducer<MovieDetailState, MovieDetailAction, MovieDetailEnvironment> {
    state, action, environment in
    struct DetailId: Hashable {}
    switch action {
    case .load:
        state.loading = true
        return environment
            .movieClient
            .detail(state.id)
            .receive(on: environment.mainQueue)
            .catchToEffect()
            .map(MovieDetailAction.detailResponse)
            .cancellable(id: DetailId())
    case .detailResponse(.success(let detail)):
        state.loading = false
        state.detail = detail
        return .none
    case .detailResponse(.failure(let error)):
        state.loading = false
        return .none
    case .onDisappear:
        return .cancel(id: DetailId())
    }
}
