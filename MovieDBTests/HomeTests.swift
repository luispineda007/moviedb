//
//  HomeTests.swift
//  MovieDBTests
//
//  Created by Luis Pineda on 2/08/21.
//

import XCTest
@testable import MovieDB
import ComposableArchitecture

class HomeTests: XCTestCase {
    let testScheduler = DispatchQueue.test
    
    func testLoadSuccess() throws {
        let store = TestStore(
            initialState: HomeState(),
            reducer: homeReducer,
            environment: HomeEnvironment(
                movieClient: .mock(movies: {_, _ in Effect(value: .mock)}),
                mainQueue: testScheduler.eraseToAnyScheduler()))
        store.assert(
            .send(.load) {
                $0.loading = true
            },
            .do { self.testScheduler.advance() },
            .receive(.movieResponse(.success(.mock))) {
                $0.loading = false
                $0.moviePage = .mock
            }
        )
    }
    
    func testLoadError() throws {
        let store = TestStore(
            initialState: HomeState(),
            reducer: homeReducer,
            environment: HomeEnvironment(
                movieClient: .mock(movies: {_, _ in Effect(error: ErrorMessage(404, "Page Not Found"))}),
                mainQueue: testScheduler.eraseToAnyScheduler()))
        store.assert(
            .send(.load) {
                $0.loading = true
            },
            .do { self.testScheduler.advance() },
            .receive(.movieResponse(.failure(ErrorMessage(404, "Page Not Found")))) {
                $0.loading = false
                //TODO: en este punto se tiene que hacer el manejo del error, nose mostrarlo en una alerta puede ser
            }
        )
    }
    
    func testLoadSuccessNextPage() throws {
        let store = TestStore(
            initialState: HomeState(moviePage: .mock(page: 1,
                                                     movies: .mock(4),
                                                     totalPages: 2,
                                                     totalResults: 8)),
            reducer: homeReducer,
            environment: HomeEnvironment(
                movieClient: .mock(movies: {_, _ in Effect(value: .mock(page: 2,
                                                                       movies: .mock(5, 8),
                                                                       totalPages: 2,
                                                                       totalResults: 8))}),
                mainQueue: testScheduler.eraseToAnyScheduler()))
        store.assert(
            .send(.nextPage) {
                $0.fielters["page"] = "2"
            },
            .receive(.movies) {
                $0.loading = true
            },
            .do { self.testScheduler.advance() },
            .receive(.movieResponse(.success(.mock(page: 2,
                                                   movies: .mock(5, 8),
                                                   totalPages: 2,
                                                   totalResults: 8)))) {
                $0.loading = false
                $0.moviePage = .mock(page: 2,
                                     movies: .mock(8),
                                     totalPages: 2,
                                     totalResults: 8)
            }
        )
    }
    
    func testSearchSuccess() throws {
        let store = TestStore(
            initialState: HomeState(),
            reducer: homeReducer,
            environment: HomeEnvironment(
                movieClient: .mock(movies: {_, _ in Effect(value: .mock)}),
                mainQueue: testScheduler.eraseToAnyScheduler()))
        store.assert(
            .send(.searchTapped) {
                $0.search = .init(query: "")
                $0.type = .search
                $0.moviePage = .init()
            },
            .send(.search(.queryChanged("ca"))) {
                $0.search?.query = "ca"
                $0.fielters["query"] = "ca"
            },
            .receive(.movies) {
                $0.loading = true
            },
            .do { self.testScheduler.advance() },
            .receive(.movieResponse(.success(.mock))) {
                $0.loading = false
                $0.moviePage = .mock
            },
            .send(.search(.cancel)) {
                $0.search = nil
                $0.type = .popular
                $0.moviePage = .init()
                $0.fielters["page"] = "1"
            },
            .receive(.movies) {
                $0.loading = true
            },
            .do { self.testScheduler.advance() },
            .receive(.movieResponse(.success(.mock))) {
                $0.loading = false
                $0.moviePage = .mock
            }
        )
        
    }
    
}































